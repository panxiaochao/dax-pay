package org.dromara.daxpay.service.controller.config;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 收银台配置
 * @author xxm
 * @since 2024/11/22
 */
@Data
@Accessors(chain = true)
@Schema(title = "收银台配置")
public class CheckoutConfig {
}
