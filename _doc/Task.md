# 单商户
## 3.0.0.bate2: 功能完善第二阶段
- 优化:
- [ ] 增加首页驾驶舱功能
- [ ] 分账功能
    - [x] 分账接收方配置
    - [ ] 分账单管理
    - [ ] 分账接口开发
- [ ] 网关配套移动端开发
    - [ ] 同步回调页
- [x] 微信通道添加单独的认证跳转地址, 处理它的特殊情况
- [ ] 支付订单新增待支付状态, 此时不需要
## 3.0.0.beta3
- [ ] 收银台台功能
  - [ ] 支持通过订单信息生成收银台链接, 
    - 进入是查询订单状态, 判断是否完成 
    - 在PC时候为收银台, 
    - 在H5时候为也是收银台, 
    - 在微信支付宝时为结算页, 可以直接发起支付
  - [ ] PC收银台, PC收银台可以生成聚合收银码, 也可以通道特殊方式的支付(微信扫码/支付宝PC支付)
  - [ ] H5收银台只在浏览器中才会出现, 在软件中会直接跳转到结算页
- [ ] 支付码牌配置
  - [x] 一个应用支持多码牌
  - [x] 码牌不再使用应用号座位标识, 使用独立的编码
  - [ ] 码牌H5页面对接

## bugs
- [x] 修复 BigDecimal 类型数据序列化和签名异常问题
- [x] 获取是否订阅消息通知类型查询范围错误问题

## 任务池
- [ ] 定时同步任务频次不要太高, 预防产生过多的数据
- [ ] 退款支持以撤销的方式进行退款
- [ ] 商户应用增加启停用状态
- [ ] 商户应用应该要有一个类似删除的功能, 实现停用冻结, 但不影响数据的关联
